# Etnetera test

### Specification
The goal of the specification is to create simple mobile application which will record a sport results and save them into the backend (firebase.com) and into the local SQLite database.
The application should be composed from two fragments and one activity. First fragment should provide a way to save a new result and the second one should provide a way to list all saved sport results. Application should use the Material design.

### Features

  - Showing the list of events
  - Adding a new event with validation (to local or remote storage)
  - Settings screen with filter & sorting preferences and storage cleanup options
  - Internalization (English, Czech and Russian)

### Tech

  - Kotlin lang
  - MVVM (on LiveData), Clean Architecture
  - Jetpack Navigation
  - RxJava3
  - Retrofit2, GSON
  - Room
  - Kodein DI
  - Constraint Layout & Material Design library

### Possible improvements

  - Event list updating on swipe down
  - Using DiffUtils with current RecyclerView
  - Hiding FAB on list scrolling
  - FireStorage Auth
  - Dialog with duration choosing form
  - Cleaning remote storage (requires Google Cloud Functions)
  - Locating user with permissions and forward-geocoding for entered location name
  - Tests