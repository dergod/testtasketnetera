package com.example.testtasketnetera

import android.app.Application
import com.example.testtasketnetera.di.DiManager
import org.kodein.di.KodeinAware
import org.kodein.di.LateInitKodein

/**
 * Application class
 */
class App : Application(), KodeinAware {

    override val kodein = LateInitKodein()

    override fun onCreate() {
        super.onCreate()
        initDi()
    }

    /**
     * Method for DI graph initialization
     */
    private fun initDi() {
        kodein.baseKodein = DiManager.init(this@App)
    }
}