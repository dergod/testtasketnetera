package com.example.testtasketnetera.data.preferences

import android.content.Context
import android.content.SharedPreferences

/**
 * Wrapper class for Android Preferences
 */
class PreferencesManager(context: Context) {

    private var prefs: SharedPreferences = context.getSharedPreferences("MyPref", 0)

    fun getBoolean(key: String): Boolean {
        return prefs.getBoolean(key, false)
    }

    fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return prefs.getBoolean(key, defaultValue)
    }

    fun setBoolean(key: String, value: Boolean) {
        prefs.edit().putBoolean(key, value).apply()
    }

    fun getString(key: String): String? {
        return prefs.getString(key, "")
    }

    fun setString(key: String, value: String) {
        prefs.edit().putString(key, value).apply()
    }

    fun getLong(key: String): Long {
        return prefs.getLong(key, -1L)
    }

    fun setLong(key: String, value: Long) {
        prefs.edit().putLong(key, value).apply()
    }

    fun getFloat(key: String): Float {
        return prefs.getFloat(key, -1.0f)
    }

    fun setFloat(key: String, value: Float) {
        prefs.edit().putFloat(key, value).apply()
    }
}