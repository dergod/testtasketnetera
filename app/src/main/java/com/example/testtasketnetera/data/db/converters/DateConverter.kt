package com.example.testtasketnetera.data.db.converters

import androidx.room.TypeConverter
import java.util.*

/**
 * Class for saving Date type to DB
 */
object DateConverter {

    @TypeConverter
    @JvmStatic
    fun fromTimestamp(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    @JvmStatic
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
}