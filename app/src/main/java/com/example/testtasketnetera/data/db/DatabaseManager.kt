package com.example.testtasketnetera.data.db

import android.content.Context
import androidx.room.Room

/**
 * Class for getting database instance
 */
class DatabaseManager(context: Context) {

    val db = Room.databaseBuilder(
        context,
        AppDatabase::class.java, "TestDatabase"
    ).build()
}