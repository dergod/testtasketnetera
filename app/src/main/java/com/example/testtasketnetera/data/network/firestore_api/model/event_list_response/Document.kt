package com.example.testtasketnetera.data.network.firestore_api.model.event_list_response

import com.example.testtasketnetera.data.network.firestore_api.model.common.Fields
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Document {

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("fields")
    @Expose
    var fields: Fields? = null

    @SerializedName("createTime")
    @Expose
    var createTime: String? = null

    @SerializedName("updateTime")
    @Expose
    var updateTime: String? = null
}