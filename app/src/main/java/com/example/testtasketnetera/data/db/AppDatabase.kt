package com.example.testtasketnetera.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.testtasketnetera.data.db.converters.DateConverter
import com.example.testtasketnetera.repository.sport_events_repository.db.SportEventDao
import com.example.testtasketnetera.repository.sport_events_repository.db.SportEventEntity

/**
 * Abstract class for DB instance with dao-getters
 */
@Database(entities = [SportEventEntity::class], version = 1, exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun sportEventDao(): SportEventDao
}