package com.example.testtasketnetera.data.network.firestore_api

import com.example.testtasketnetera.data.network.firestore_api.model.add_event_request.AddEventRequest
import com.example.testtasketnetera.data.network.firestore_api.model.event_list_response.EventListResponse
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Class, which describes Cloud Firestore API
 */
interface FirestoreApi {

    /**
     * Method for getting all events
     */
    @GET("projects/etnetera-test/databases/(default)/documents/events")
    fun getEvents(
        @Query("key") apiKey: String
    ): Single<EventListResponse>

    /**
     * Method for adding new event
     */
    @POST("projects/etnetera-test/databases/(default)/documents/events")
    fun addEvent(
        @Query("documentId") eventName: String,
        @Query("key") apiKey: String,
        @Body content: AddEventRequest
    ): Completable
}