package com.example.testtasketnetera.data.network.firestore_api.model.common

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Duration {

    @SerializedName("stringValue")
    @Expose
    var stringValue: String? = null
}