package com.example.testtasketnetera.data.network.firestore_api.model.common

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CreatedDate {

    @SerializedName("timestampValue")
    @Expose
    var timestampValue: String? = null
}