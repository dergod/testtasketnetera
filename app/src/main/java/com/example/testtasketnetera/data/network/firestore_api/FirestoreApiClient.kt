package com.example.testtasketnetera.data.network.firestore_api

import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Class for getting client
 * to interact with Cloud Firestore
 */
class FirestoreApiClient {

    companion object {
        private const val BASE_URL = "https://firestore.googleapis.com/v1/"
        const val API_KEY = "AIzaSyC3xDesx-dlc86P9p-mXrqo9iXc5LA2NYk"
    }

    val instance: FirestoreApi by lazy { getClient() }

    /**
     * Retrofit init method
     */
    private fun getClient(): FirestoreApi {
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .baseUrl(BASE_URL)
            .build()
        return retrofit.create(FirestoreApi::class.java)
    }
}