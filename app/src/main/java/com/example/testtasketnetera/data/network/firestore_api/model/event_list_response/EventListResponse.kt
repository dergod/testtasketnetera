package com.example.testtasketnetera.data.network.firestore_api.model.event_list_response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Simple model for parsing response with all events
 */
class EventListResponse {

    @SerializedName("documents")
    @Expose
    var documents: List<Document>? = null
}