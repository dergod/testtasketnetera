package com.example.testtasketnetera.data.network.firestore_api.model.common

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Fields {

    @SerializedName("duration")
    @Expose
    var duration: Duration? = null

    @SerializedName("location")
    @Expose
    var location: Location? = null

    @SerializedName("name")
    @Expose
    var name: Name? = null

    @SerializedName("createdDate")
    @Expose
    var createdDate: CreatedDate? = null
}