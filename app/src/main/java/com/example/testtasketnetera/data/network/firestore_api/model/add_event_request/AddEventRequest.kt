package com.example.testtasketnetera.data.network.firestore_api.model.add_event_request

import com.example.testtasketnetera.data.network.firestore_api.model.common.Fields
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Class for sending event content as parameter
 */
class AddEventRequest {

    @SerializedName("fields")
    @Expose
    var fields: Fields? = null
}