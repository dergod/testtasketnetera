package com.example.testtasketnetera.repository.storage_repository.model

/**
 * Contains all possible types of storage
 */
enum class StorageTypeEnum {
    LOCAL,
    REMOTE
}