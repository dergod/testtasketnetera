package com.example.testtasketnetera.repository.sport_events_repository.model

/**
 * Contains all possible types of sorting events
 */
enum class SortingTypeEnum {
    DATE,
    NAME,
    LOCATION,
    DURATION
}