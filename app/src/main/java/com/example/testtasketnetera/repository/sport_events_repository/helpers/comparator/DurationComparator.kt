package com.example.testtasketnetera.repository.sport_events_repository.helpers.comparator

import com.example.testtasketnetera.repository.sport_events_repository.model.SportEventModel

/**
 * Class for comparing events by duration
 * (duration string actually - in current realization)
 */
class DurationComparator : Comparator<SportEventModel> {

    override fun compare(p0: SportEventModel?, p1: SportEventModel?): Int {
        val firstObjectDuration = p0?.duration
        val secondObjectDuration = p1?.duration
        if (firstObjectDuration == secondObjectDuration) {
            return 0
        }
        if (firstObjectDuration == null) {
            return -1
        }
        if (secondObjectDuration == null) {
            return 1
        }
        return firstObjectDuration.compareTo(secondObjectDuration)
    }
}