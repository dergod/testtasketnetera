package com.example.testtasketnetera.repository.sport_events_repository.model

import com.example.testtasketnetera.repository.location_repository.model.LocationModel
import com.example.testtasketnetera.repository.storage_repository.model.StorageTypeEnum
import java.util.*

/**
 * Model for working with sport results
 */
data class SportEventModel(
    val name: String,
    val duration: String,
    val location: LocationModel,
    val storageType: StorageTypeEnum,
    val createdDate: Date
)