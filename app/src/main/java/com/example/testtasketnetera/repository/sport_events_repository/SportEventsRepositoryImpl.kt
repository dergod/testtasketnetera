package com.example.testtasketnetera.repository.sport_events_repository

import com.example.testtasketnetera.data.db.DatabaseManager
import com.example.testtasketnetera.data.network.firestore_api.FirestoreApiClient
import com.example.testtasketnetera.data.preferences.PreferencesManager
import com.example.testtasketnetera.repository.sport_events_repository.helpers.EventListHandler
import com.example.testtasketnetera.repository.sport_events_repository.helpers.SportEventsMapper
import com.example.testtasketnetera.repository.sport_events_repository.model.SortingTypeEnum
import com.example.testtasketnetera.repository.sport_events_repository.model.SportEventModel
import com.example.testtasketnetera.repository.storage_repository.model.StorageTypeEnum
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

/**
 * Class which encapsulates db and network operations
 * related to sport events
 */
class SportEventsRepositoryImpl(
    private val apiClient: FirestoreApiClient,
    private val databaseManager: DatabaseManager,
    private val prefsManager: PreferencesManager
) : SportEventsRepository {

    companion object {
        private const val PREFS_KEY_SHOW_LOCAL_EVENTS = "show_local_events"
        private const val PREFS_KEY_SHOW_REMOTE_EVENTS = "show_remote_events"
        private const val PREFS_KEY_EVENTS_SORTING_TYPE = "events_sorting_type"
    }

    /**
     * Method for getting all sport events
     * @return rx observable with sport event list
     */
    override fun getSportEvents(): Single<List<SportEventModel>> {
        val networkObservable = apiClient.instance.getEvents(FirestoreApiClient.API_KEY)
            .map { response -> SportEventsMapper.mapResponseToDomain(response) }
        val dbObservable = Single.fromCallable {
            databaseManager.db.sportEventDao().getAll().toMutableList()
                .map { entity -> SportEventsMapper.mapDbEntityToDomain(entity) }
        }
        return networkObservable.zipWith(dbObservable, { networkResult, dbResult ->
            val resultList = mutableListOf<SportEventModel>()
            resultList.addAll(networkResult)
            resultList.addAll(dbResult)
            resultList.toList()
        })
            .map { eventList -> EventListHandler.filterLocalEvents(this, eventList) }
            .map { eventList -> EventListHandler.filterRemoteEvents(this, eventList) }
            .map { eventList -> EventListHandler.sortEvents(this, eventList) }
    }

    /**
     * Method for saving new sport event
     * to local or remote storage
     */
    override fun saveNewEvent(event: SportEventModel): Completable {
        val networkObservable = apiClient.instance.addEvent(
            event.name,
            FirestoreApiClient.API_KEY,
            SportEventsMapper.mapDomainToRequest(event)
        )
        val dbObservable = Completable.fromCallable {
            databaseManager.db.sportEventDao()
                .insertAll(SportEventsMapper.mapDomainModelToDbEntity(event))
        }
        return when (event.storageType) {
            StorageTypeEnum.LOCAL -> dbObservable
            StorageTypeEnum.REMOTE -> networkObservable
        }
    }

    /**
     * Method for deleting all local events
     */
    override fun deleteLocalEvents(): Completable {
        return Completable.fromCallable {
            databaseManager.db.sportEventDao().clearTable()
        }
    }

    /**
     * Method for deleting all remote events
     */
    override fun deleteRemoteEvents() {
        // todo - can be implemented with Google cloud functions
    }

    /**
     * Method for getting current local events display mode
     * @return is visible
     */
    override fun isNeedToShowEventsFromLocalStorage(): Boolean {
        return prefsManager.getBoolean(PREFS_KEY_SHOW_LOCAL_EVENTS, true)
    }

    /**
     * Method to switch local events display mode
     * @param needToShow - show local events if it's true
     */
    override fun showEventsFromLocalStorage(needToShow: Boolean) {
        prefsManager.setBoolean(PREFS_KEY_SHOW_LOCAL_EVENTS, needToShow)
    }

    /**
     * Method for getting current remote events display mode
     * @return is visible
     */
    override fun isNeedToShowEventsFromRemoteStorage(): Boolean {
        return prefsManager.getBoolean(PREFS_KEY_SHOW_REMOTE_EVENTS, true)
    }

    /**
     * Method to switch remote events display mode
     * @param needToShow - show remote events if it's true
     */
    override fun showEventsFromRemoteStorage(needToShow: Boolean) {
        prefsManager.setBoolean(PREFS_KEY_SHOW_REMOTE_EVENTS, needToShow)
    }

    /**
     * Method for getting sorting type of events
     */
    override fun getCurrentSortingType(): SortingTypeEnum {
        var stringValue = prefsManager.getString(PREFS_KEY_EVENTS_SORTING_TYPE)
        if (stringValue.isNullOrEmpty()) stringValue = SortingTypeEnum.DATE.name
        return SortingTypeEnum.valueOf(stringValue)
    }

    /**
     * Method for setting sorting type of events
     */
    override fun setCurrentSortingType(sortingType: SortingTypeEnum) {
        prefsManager.setString(PREFS_KEY_EVENTS_SORTING_TYPE, sortingType.name)
    }
}