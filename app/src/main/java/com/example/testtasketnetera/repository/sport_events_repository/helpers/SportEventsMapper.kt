package com.example.testtasketnetera.repository.sport_events_repository.helpers

import com.example.testtasketnetera.data.network.firestore_api.model.add_event_request.AddEventRequest
import com.example.testtasketnetera.data.network.firestore_api.model.common.*
import com.example.testtasketnetera.data.network.firestore_api.model.event_list_response.EventListResponse
import com.example.testtasketnetera.repository.location_repository.model.LocationModel
import com.example.testtasketnetera.repository.sport_events_repository.db.SportEventEntity
import com.example.testtasketnetera.repository.sport_events_repository.model.SportEventModel
import com.example.testtasketnetera.repository.storage_repository.model.StorageTypeEnum
import java.util.*

/**
 * Singleton for mapping domain types to data types and vice versa
 */
object SportEventsMapper {

    /**
     * Method for mapping network response to domain models
     */
    fun mapResponseToDomain(response: EventListResponse): List<SportEventModel> {
        val documents = response.documents
        documents ?: return emptyList()
        val result = mutableListOf<SportEventModel>()
        for (document in documents) {
            val eventName = document.fields?.name?.stringValue ?: ""
            val eventDuration = document.fields?.duration?.stringValue ?: ""
            val eventLocation = document.fields?.location?.stringValue ?: ""
            val eventCreatedDate = document.fields?.createdDate?.timestampValue ?: ""
            val event = SportEventModel(
                eventName,
                eventDuration,
                LocationModel(eventLocation, 0.0, 0.0),
                StorageTypeEnum.REMOTE,
                SportEventsDateParser.parseDate(eventCreatedDate)
            )
            result.add(event)
        }
        return result
    }

    /**
     * Method for mapping domain model to network request model
     */
    fun mapDomainToRequest(model: SportEventModel): AddEventRequest {
        return model.run {
            val request = AddEventRequest()
            val fields = Fields()

            val nameField = Name()
            nameField.stringValue = name
            fields.name = nameField

            val durationField = Duration()
            durationField.stringValue = duration
            fields.duration = durationField

            val locationField = Location()
            locationField.stringValue = location.name
            fields.location = locationField

            val createdDateField = CreatedDate()
            createdDateField.timestampValue = SportEventsDateParser.convertDateToString(createdDate)
            fields.createdDate = createdDateField

            request.fields = fields
            request
        }
    }

    /**
     * Method for mapping db entity to domain model
     */
    fun mapDbEntityToDomain(entity: SportEventEntity): SportEventModel {
        return entity.run {
            SportEventModel(
                name = name ?: "",
                duration = duration ?: "",
                location = LocationModel(location ?: "", 0.0, 0.0),
                storageType = StorageTypeEnum.LOCAL,
                createdDate = createdDate
            )
        }
    }

    /**
     * Method for mapping event domain model to db entity model
     */
    fun mapDomainModelToDbEntity(model: SportEventModel): SportEventEntity {
        return model.run {
            SportEventEntity(
                uid = Random().nextInt(),
                name = name,
                location = location.name,
                duration = duration,
                createdDate = createdDate
            )
        }
    }
}