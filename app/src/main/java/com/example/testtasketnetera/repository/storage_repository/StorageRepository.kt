package com.example.testtasketnetera.repository.storage_repository

import io.reactivex.rxjava3.core.Completable

/**
 * Facade class for storage types business logic
 */
interface StorageRepository {

    /**
     * Method for clearing local storage
     */
    fun clearLocalStorage(): Completable

    /**
     * Method for clearing remote storage
     */
    fun clearRemoteStorage()
}