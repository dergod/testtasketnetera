package com.example.testtasketnetera.repository.storage_repository

import com.example.testtasketnetera.repository.sport_events_repository.SportEventsRepositoryImpl
import io.reactivex.rxjava3.core.Completable

/**
 * Facade class for storage types business logic
 */
class StorageRepositoryImpl(private val eventsRepo: SportEventsRepositoryImpl) : StorageRepository {

    /**
     * Method for clearing local storage
     */
    override fun clearLocalStorage(): Completable {
        return eventsRepo.deleteLocalEvents() // other repos can be here
    }

    /**
     * Method for clearing remote storage
     */
    override fun clearRemoteStorage() {
        // todo - can be implemented with Google cloud functions
    }
}