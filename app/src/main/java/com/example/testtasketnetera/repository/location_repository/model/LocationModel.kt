package com.example.testtasketnetera.repository.location_repository.model

/**
 * Model for working with locations
 */
data class LocationModel(
    val name: String,
    val lat: Double,
    val lon: Double
)