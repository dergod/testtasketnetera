package com.example.testtasketnetera.repository.sport_events_repository.helpers

import java.text.SimpleDateFormat
import java.util.*

/**
 * Helper class for parsing dates
 */
object SportEventsDateParser {

    private const val FIREBASE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"

    /**
     * Parse input string to date type
     */
    fun parseDate(input: String): Date {
        if (input.isEmpty()) return Date()
        return getFirebaseFormatter().parse(input) ?: Date()
    }

    fun convertDateToString(date: Date): String {
        return getFirebaseFormatter().format(date)
    }

    private fun getFirebaseFormatter(): SimpleDateFormat {
        val firebaseFormatter = SimpleDateFormat(FIREBASE_TIME_FORMAT, Locale.ENGLISH)
        firebaseFormatter.timeZone = TimeZone.getTimeZone("GMT")
        return firebaseFormatter
    }
}