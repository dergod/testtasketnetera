package com.example.testtasketnetera.repository.sport_events_repository.helpers.comparator

import com.example.testtasketnetera.repository.sport_events_repository.model.SportEventModel

/**
 * Class for comparing events by location
 * (location name actually - in current realization)
 */
class LocationComparator : Comparator<SportEventModel> {

    override fun compare(p0: SportEventModel?, p1: SportEventModel?): Int {
        val firstObjectLocation = p0?.location?.name
        val secondObjectLocation = p1?.location?.name
        if (firstObjectLocation == secondObjectLocation) {
            return 0
        }
        if (firstObjectLocation == null) {
            return -1
        }
        if (secondObjectLocation == null) {
            return 1
        }
        return firstObjectLocation.compareTo(secondObjectLocation)
    }
}