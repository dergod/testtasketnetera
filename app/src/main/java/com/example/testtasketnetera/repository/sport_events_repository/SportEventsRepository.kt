package com.example.testtasketnetera.repository.sport_events_repository

import com.example.testtasketnetera.repository.sport_events_repository.model.SortingTypeEnum
import com.example.testtasketnetera.repository.sport_events_repository.model.SportEventModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

/**
 * Class which encapsulates db and network operations
 * related to sport events
 */
interface SportEventsRepository {

    /**
     * Method for getting all sport events
     * @return rx observable with sport event list
     */
    fun getSportEvents(): Single<List<SportEventModel>>

    /**
     * Method for saving new sport event
     * to local or remote storage
     */
    fun saveNewEvent(event: SportEventModel): Completable

    /**
     * Method for deleting all local events
     */
    fun deleteLocalEvents(): Completable

    /**
     * Method for deleting all remote events
     */
    fun deleteRemoteEvents()

    /**
     * Method for getting current local events display mode
     * @return is visible
     */
    fun isNeedToShowEventsFromLocalStorage(): Boolean

    /**
     * Method to switch local events display mode
     * @param needToShow - show local events if it's true
     */
    fun showEventsFromLocalStorage(needToShow: Boolean)

    /**
     * Method for getting current remote events display mode
     * @return is visible
     */
    fun isNeedToShowEventsFromRemoteStorage(): Boolean

    /**
     * Method to switch remote events display mode
     * @param needToShow - show remote events if it's true
     */
    fun showEventsFromRemoteStorage(needToShow: Boolean)

    /**
     * Method for getting sorting type of events
     */
    fun getCurrentSortingType(): SortingTypeEnum

    /**
     * Method for setting sorting type of events
     */
    fun setCurrentSortingType(sortingType: SortingTypeEnum)
}