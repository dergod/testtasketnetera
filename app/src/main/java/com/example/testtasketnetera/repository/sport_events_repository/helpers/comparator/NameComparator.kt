package com.example.testtasketnetera.repository.sport_events_repository.helpers.comparator

import com.example.testtasketnetera.repository.sport_events_repository.model.SportEventModel

/**
 * Class for comparing events by name
 */
class NameComparator : Comparator<SportEventModel> {

    override fun compare(p0: SportEventModel?, p1: SportEventModel?): Int {
        val firstObjectName = p0?.name
        val secondObjectName = p1?.name
        if (firstObjectName == secondObjectName) {
            return 0
        }
        if (firstObjectName == null) {
            return -1
        }
        if (secondObjectName == null) {
            return 1
        }
        return firstObjectName.compareTo(secondObjectName)
    }
}