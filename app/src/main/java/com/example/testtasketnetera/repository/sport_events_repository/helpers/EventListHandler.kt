package com.example.testtasketnetera.repository.sport_events_repository.helpers

import com.example.testtasketnetera.repository.sport_events_repository.SportEventsRepositoryImpl
import com.example.testtasketnetera.repository.sport_events_repository.helpers.comparator.DateComparator
import com.example.testtasketnetera.repository.sport_events_repository.helpers.comparator.DurationComparator
import com.example.testtasketnetera.repository.sport_events_repository.helpers.comparator.LocationComparator
import com.example.testtasketnetera.repository.sport_events_repository.helpers.comparator.NameComparator
import com.example.testtasketnetera.repository.sport_events_repository.model.SortingTypeEnum
import com.example.testtasketnetera.repository.sport_events_repository.model.SportEventModel
import com.example.testtasketnetera.repository.storage_repository.model.StorageTypeEnum

/**
 * Class for processing event list
 */
object EventListHandler {

    fun filterLocalEvents(
        repository: SportEventsRepositoryImpl,
        eventList: List<SportEventModel>
    ): List<SportEventModel> {
        if (!repository.isNeedToShowEventsFromLocalStorage()) {
            return eventList.filter { model ->
                model.storageType != StorageTypeEnum.LOCAL
            }
        }
        return eventList
    }

    fun filterRemoteEvents(
        repository: SportEventsRepositoryImpl,
        eventList: List<SportEventModel>
    ): List<SportEventModel> {
        if (!repository.isNeedToShowEventsFromRemoteStorage()) {
            return eventList.filter { model ->
                model.storageType != StorageTypeEnum.REMOTE
            }
        }
        return eventList
    }

    fun sortEvents(
        repository: SportEventsRepositoryImpl,
        eventList: List<SportEventModel>
    ): List<SportEventModel> {
        return when (repository.getCurrentSortingType()) {
            SortingTypeEnum.DATE -> {
                eventList.sortedWith(DateComparator())
            }
            SortingTypeEnum.NAME -> {
                eventList.sortedWith(NameComparator())
            }
            SortingTypeEnum.LOCATION -> {
                eventList.sortedWith(LocationComparator())
            }
            SortingTypeEnum.DURATION -> {
                eventList.sortedWith(DurationComparator())
            }
        }
    }
}