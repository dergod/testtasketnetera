package com.example.testtasketnetera.repository.sport_events_repository.helpers.comparator

import com.example.testtasketnetera.repository.sport_events_repository.model.SportEventModel

/**
 * Class for comparing events by created date
 */
class DateComparator : Comparator<SportEventModel> {

    override fun compare(p0: SportEventModel?, p1: SportEventModel?): Int {
        val firstObjectCreatedDate = p0?.createdDate
        val secondObjectCreatedDate = p1?.createdDate

        if (firstObjectCreatedDate == null) return 1
        if (secondObjectCreatedDate == null) return -1

        return when {
            firstObjectCreatedDate.before(secondObjectCreatedDate) -> 1
            firstObjectCreatedDate.after(secondObjectCreatedDate) -> -1
            else -> 0
        }
    }
}