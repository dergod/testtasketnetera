package com.example.testtasketnetera.repository.sport_events_repository.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

/**
 * Entity class for sport event DB table
 */
@Entity(tableName = "sport_events")
data class SportEventEntity(
    @PrimaryKey val uid: Int,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "location") val location: String?,
    @ColumnInfo(name = "duration") val duration: String?,
    @ColumnInfo(name = "created_date") val createdDate: Date
)