package com.example.testtasketnetera.repository.sport_events_repository.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

/**
 * Dao class (it's for working with sport event entity)
 */
@Dao
interface SportEventDao {

    @Query("SELECT * FROM sport_events")
    fun getAll(): List<SportEventEntity>

    @Query("SELECT * FROM sport_events WHERE uid IN (:eventIds)")
    fun loadAllByIds(eventIds: IntArray): List<SportEventEntity>

    @Query("SELECT * FROM sport_events WHERE name LIKE :name LIMIT 1")
    fun findByName(name: String): SportEventEntity

    @Insert
    fun insertAll(vararg users: SportEventEntity)

    @Delete
    fun delete(event: SportEventEntity)

    @Query("DELETE FROM sport_events")
    fun clearTable()
}