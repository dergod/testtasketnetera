package com.example.testtasketnetera.utils

import android.util.Log
import com.example.testtasketnetera.BuildConfig

/**
 * Simple class for encapsulating logging logic
 */
object LoggingUtils {

    /**
     * Method for logging exceptions
     * @param e error
     */
    fun log(e: Throwable) {
        if (BuildConfig.DEBUG) {
            Log.d("Something's strange", e.message ?: "")
            e.printStackTrace()
        }
    }
}