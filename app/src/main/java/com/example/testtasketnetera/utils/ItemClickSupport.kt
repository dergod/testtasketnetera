package com.example.testtasketnetera.utils

import android.view.View
import android.view.View.OnLongClickListener
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnChildAttachStateChangeListener
import com.example.testtasketnetera.R

/*
  Source: http://www.littlerobots.nl/blog/Handle-Android-RecyclerView-Clicks/
  https://gist.github.com/nesquena/231e356f372f214c4fe6
  USAGE:

  ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
      @Override
      public void onItemClicked(RecyclerView recyclerView, int position, View v) {
          // do it
      }
  });
*/
class ItemClickSupport(recyclerView: RecyclerView) {

    companion object {

        fun addTo(view: RecyclerView): ItemClickSupport? {
            var support = view.getTag(R.id.item_click_support) as ItemClickSupport?
            if (support == null) {
                support =
                    ItemClickSupport(view)
            }
            return support
        }

        fun removeFrom(view: RecyclerView): ItemClickSupport? {
            val support = view.getTag(R.id.item_click_support) as ItemClickSupport?
            support?.detach(view)
            return support
        }
    }

    private var mRecyclerView: RecyclerView? = recyclerView

    internal var mOnItemClickListener: OnItemClickListener? = null

    internal var mOnItemLongClickListener: OnItemLongClickListener? = null

    internal val mOnClickListener: View.OnClickListener = View.OnClickListener { v ->
        if (mOnItemClickListener != null) {
            val holder = mRecyclerView!!.getChildViewHolder(v)
            mOnItemClickListener!!.onItemClicked(mRecyclerView, holder.adapterPosition, v)
        }
    }

    internal val mOnLongClickListener = OnLongClickListener { v ->
        if (mOnItemLongClickListener != null) {
            val holder = mRecyclerView!!.getChildViewHolder(v)
            return@OnLongClickListener mOnItemLongClickListener!!.onItemLongClicked(
                mRecyclerView,
                holder.adapterPosition,
                v
            )
        }
        false
    }

    private val mAttachListener: OnChildAttachStateChangeListener =
        object : OnChildAttachStateChangeListener {
            override fun onChildViewDetachedFromWindow(view: View) {}

            override fun onChildViewAttachedToWindow(view: View) {
                if (mOnItemClickListener != null) {
                    view.setOnClickListener(mOnClickListener)
                }
                if (mOnItemLongClickListener != null) {
                    view.setOnLongClickListener(mOnLongClickListener)
                }
            }
        }

    fun setOnItemClickListener(listener: OnItemClickListener?): ItemClickSupport? {
        mOnItemClickListener = listener
        return this
    }

    fun setOnItemLongClickListener(listener: OnItemLongClickListener?): ItemClickSupport? {
        mOnItemLongClickListener = listener
        return this
    }

    private fun detach(view: RecyclerView) {
        view.removeOnChildAttachStateChangeListener(mAttachListener)
        view.setTag(R.id.item_click_support, null)
    }

    interface OnItemClickListener {
        fun onItemClicked(recyclerView: RecyclerView?, position: Int, v: View?)
    }

    interface OnItemLongClickListener {
        fun onItemLongClicked(
            recyclerView: RecyclerView?,
            position: Int,
            v: View?
        ): Boolean
    }

    init {
        mRecyclerView?.setTag(R.id.item_click_support, this)
        mRecyclerView?.addOnChildAttachStateChangeListener(mAttachListener)
    }
}