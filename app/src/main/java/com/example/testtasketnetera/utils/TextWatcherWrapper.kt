package com.example.testtasketnetera.utils

import android.text.Editable
import android.text.TextWatcher

/**
 * For more clean code
 */
abstract class TextWatcherWrapper : TextWatcher {

    override fun afterTextChanged(p0: Editable?) {
        // Не используется
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        // Не используется
    }

    abstract override fun onTextChanged(newValue: CharSequence?, p1: Int, p2: Int, p3: Int)
}