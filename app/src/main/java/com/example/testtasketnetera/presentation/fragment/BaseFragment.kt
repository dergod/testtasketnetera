package com.example.testtasketnetera.presentation.fragment

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

/**
 * Parent fragment for all fragments
 */
open class BaseFragment(@LayoutRes layoutId: Int) : Fragment(layoutId) {

    companion object {
        private const val SHOW_KEYBOARD_TIMER = 100L
    }

    /**
     * Show keyboard method
     */
    fun showKeyboard(context: Context?, view: View) {
        val imm = context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }

    /**
     * Show keyboard method (with dirty hack)
     */
    fun showKeyboardPostDelayed(context: Context?, view: View) {
        view.postDelayed({
            view.requestFocus()
            showKeyboard(context, view)
        }, SHOW_KEYBOARD_TIMER)
    }

    /**
     * Hide keyboard method
     */
    fun hideKeyboardFrom(context: Context?, view: View) {
        val imm = context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(view.windowToken, 0)
    }
}