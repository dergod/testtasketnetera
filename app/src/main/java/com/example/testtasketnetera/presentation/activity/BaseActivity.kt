package com.example.testtasketnetera.presentation.activity

import android.annotation.SuppressLint
import androidx.fragment.app.FragmentActivity

/**
 * Parent activity for all activities
 */
@SuppressLint("Registered")
open class BaseActivity : FragmentActivity()