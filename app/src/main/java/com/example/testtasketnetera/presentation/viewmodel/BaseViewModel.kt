package com.example.testtasketnetera.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.testtasketnetera.App
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware

/**
 * Parent view model for all view models
 * @param app - application instance
 */
open class BaseViewModel(private val app: Application) : AndroidViewModel(app), KodeinAware {

    override val kodein: Kodein
        get() = (app as App).kodein
}