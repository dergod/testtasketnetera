package com.example.testtasketnetera.presentation.mapper

import com.example.testtasketnetera.presentation.model.SportEventPresentationModel
import com.example.testtasketnetera.repository.location_repository.model.LocationModel
import com.example.testtasketnetera.repository.sport_events_repository.model.SportEventModel

/**
 * Class for mapping domain-related models to presentation-related
 */
object EventsMapper {

    fun mapEventList(list: List<SportEventModel>): List<SportEventPresentationModel> {
        return list.map {
            it.run {
                SportEventPresentationModel(
                    name = name,
                    location = location.name,
                    duration = duration,
                    storageType = storageType,
                    createdDate = createdDate
                )
            }
        }
    }

    fun mapNewEvent(event: SportEventPresentationModel): SportEventModel {
        return event.run {
            SportEventModel(
                name = name,
                duration = duration,
                location = LocationModel(location, 0.0, 0.0),
                storageType = storageType,
                createdDate = createdDate
            )
        }
    }
}