package com.example.testtasketnetera.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.testtasketnetera.presentation.mapper.EventsMapper
import com.example.testtasketnetera.presentation.model.SportEventPresentationModel
import com.example.testtasketnetera.repository.sport_events_repository.SportEventsRepository
import com.example.testtasketnetera.repository.sport_events_repository.model.SportEventModel
import com.example.testtasketnetera.repository.storage_repository.model.StorageTypeEnum
import com.example.testtasketnetera.utils.LoggingUtils
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.kodein.di.generic.instance
import java.util.*

/**
 * View model for AddEventFragment
 */
class AddEventViewModel(app: Application) : BaseViewModel(app) {

    private val eventsRepository: SportEventsRepository by instance()
    private val compositeDisposable = CompositeDisposable()

    var currentName = ""
    var currentLocationName = ""
    var currentDuration = ""
    var storageType = StorageTypeEnum.LOCAL

    /**
     * LiveData for indicating that sending was successful
     */
    private val _inSuccessState = MutableLiveData<Boolean>()
    val inSuccessState: LiveData<Boolean> = _inSuccessState

    /**
     * LiveData for indicating progress state on AddEventFragment
     */
    private val _inLoadingState = MutableLiveData<Boolean>()
    val inLoadingState: LiveData<Boolean> = _inLoadingState

    /**
     * LiveData for indicating validation error state on AddEventFragment
     */
    private val _inValidationErrorState = MutableLiveData<Boolean>()
    val inValidationErrorState: LiveData<Boolean> = _inValidationErrorState

    /**
     * LiveData for indicating error state on AddEventFragment
     */
    private val _inErrorState = MutableLiveData<Boolean>()
    val inErrorState: LiveData<Boolean> = _inErrorState

    /**
     * Method for sending new data to repository
     */
    fun onSendButtonClicked() {
        if (currentName.isEmpty()) {
            _inValidationErrorState.value = true
            return
        }
        _inLoadingState.value = true
        val disposable = eventsRepository.saveNewEvent(getNewEventModel())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _inSuccessState.value = true
            }, { error ->
                _inErrorState.value = true
                LoggingUtils.log(error)
            })
        compositeDisposable.add(disposable)
    }

    private fun getNewEventModel(): SportEventModel {
        val newEvent = SportEventPresentationModel(
            currentName,
            currentLocationName,
            currentDuration,
            storageType,
            Date()
        )
        return EventsMapper.mapNewEvent(newEvent)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}