package com.example.testtasketnetera.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.testtasketnetera.presentation.mapper.EventsMapper
import com.example.testtasketnetera.presentation.model.SportEventPresentationModel
import com.example.testtasketnetera.repository.sport_events_repository.SportEventsRepository
import com.example.testtasketnetera.utils.LoggingUtils
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.kodein.di.generic.instance

/**
 * View model for EventListFragment
 */
class EventListViewModel(app: Application) : BaseViewModel(app) {

    private val eventsRepository: SportEventsRepository by instance()
    private val compositeDisposable = CompositeDisposable()

    /**
     * LiveData for indicating data state on EventListFragment
     */
    private val _inDataState = MutableLiveData<List<SportEventPresentationModel>>()
    val inDataState: LiveData<List<SportEventPresentationModel>> = _inDataState

    /**
     * LiveData for indicating progress state on EventListFragment
     */
    private val _inLoadingState = MutableLiveData<Boolean>()
    val inLoadingState: LiveData<Boolean> = _inLoadingState

    /**
     * LiveData for indicating error state on EventListFragment
     */
    private val _inErrorState = MutableLiveData<Boolean>()
    val inErrorState: LiveData<Boolean> = _inErrorState

    /**
     * Method for getting new data from repository
     */
    fun requestData() {
        _inLoadingState.value = true
        val disposable = eventsRepository.getSportEvents()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { domainModels -> EventsMapper.mapEventList(domainModels) }
            .subscribe({ data ->
                _inDataState.value = data
            }, { error ->
                _inErrorState.value = true
                LoggingUtils.log(error)
            })
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}