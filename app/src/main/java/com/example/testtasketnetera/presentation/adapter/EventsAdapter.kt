package com.example.testtasketnetera.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.testtasketnetera.R
import com.example.testtasketnetera.repository.storage_repository.model.StorageTypeEnum
import com.example.testtasketnetera.presentation.model.SportEventPresentationModel

/**
 * Adapter class for sport events
 */
class EventsAdapter : RecyclerView.Adapter<EventsAdapter.ViewHolder>() {

    var context: Context? = null
    var events: List<SportEventPresentationModel>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val eventsView = inflater.inflate(R.layout.item_event, parent, false)
        return ViewHolder(eventsView)
    }

    override fun getItemCount(): Int = events?.size ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        events?.let {
            val event = it[position]

            val name = holder.nameTextView
            name.text = event.name

            val location = holder.locationTextView
            location.text = event.location

            val duration = holder.durationTextView
            duration.text = event.duration

            val storageType = holder.storageTypeTextView
            when (event.storageType) {
                StorageTypeEnum.LOCAL -> configureLocalLabel(storageType)
                StorageTypeEnum.REMOTE -> configureRemoteLabel(storageType)
            }
        }
    }

    private fun configureLocalLabel(storageType: TextView) {
        context?.let { nonNullContext ->
            storageType.text =
                nonNullContext.getString(R.string.data_local_storage_label)
            storageType.background = ContextCompat.getDrawable(
                nonNullContext,
                R.drawable.label_background_accent
            )
        }
    }

    private fun configureRemoteLabel(storageType: TextView) {
        context?.let { nonNullContext ->
            storageType.text = nonNullContext.getString(R.string.data_remote_storage_label)
            storageType.background = ContextCompat.getDrawable(
                nonNullContext,
                R.drawable.label_background_primary
            )
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameTextView: TextView = itemView.findViewById(R.id.itemEventName)
        var locationTextView: TextView = itemView.findViewById(R.id.itemEventLocation)
        var durationTextView: TextView = itemView.findViewById(R.id.itemEventDuration)
        var storageTypeTextView: TextView = itemView.findViewById(R.id.itemEventStorageType)
    }
}