package com.example.testtasketnetera.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.testtasketnetera.utils.SingleLiveEvent

/**
 * Main activity view model
 */
class MainViewModel(app: Application) : BaseViewModel(app) {

    /**
     * LiveData for sending navigation event
     * to go to event list
     */
    private val _navigateToEventList = SingleLiveEvent<Unit>()
    val navigateToEventList: LiveData<Unit> = _navigateToEventList

    /**
     * LiveData for sending navigation event
     * to go to add event screen
     */
    private val _navigateToAddEvent = SingleLiveEvent<Unit>()
    val navigateToAddEvent: LiveData<Unit> = _navigateToAddEvent

    /**
     * Triggers main bottom menu visibility
     * true - visible
     * false - gone
     */
    private val _menuVisibility = SingleLiveEvent<Boolean>()
    val menuVisibility: LiveData<Boolean> = _menuVisibility

    fun navigateFromAddEventToEventListFragment() {
        _menuVisibility.value = true
        _navigateToEventList.call()
    }

    fun navigateToAddEventFragment() {
        _menuVisibility.value = false
        _navigateToAddEvent.call()
    }
}