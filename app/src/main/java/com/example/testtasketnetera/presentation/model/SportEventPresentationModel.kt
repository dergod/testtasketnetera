package com.example.testtasketnetera.presentation.model

import com.example.testtasketnetera.repository.storage_repository.model.StorageTypeEnum
import java.util.*

/**
 * Model for working with sport results (presentation layer)
 */
data class SportEventPresentationModel(
    val name: String,
    val location: String,
    val duration: String,
    val storageType: StorageTypeEnum,
    val createdDate: Date
)