package com.example.testtasketnetera.presentation.activity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.testtasketnetera.R
import com.example.testtasketnetera.databinding.ActivityMainBinding
import com.example.testtasketnetera.presentation.viewmodel.MainViewModel

/**
 * The main activity of this app
 */
class MainActivity : BaseActivity() {

    companion object {
        private const val ANIMATION_VISIBLE_ALPHA = 1.0f
        private const val ANIMATION_GONE_ALPHA = 0.0f
        private const val ANIMATION_MENU_DURATION = 500L
    }

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        configureNavigation()
        subscribeToViewModelEvents()
    }

    private fun configureNavigation() {
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.mainContainer) as NavHostFragment
        binding.mainMenu.setupWithNavController(navHostFragment.navController)
    }

    private fun subscribeToViewModelEvents() {
        viewModel.navigateToEventList.observe(this, {
            navigateFromAddEventToEventList()
        })
        viewModel.navigateToAddEvent.observe(this, {
            navigateToAddEvent()
        })
        viewModel.menuVisibility.observe(this, { isVisible ->
            if (isVisible) showBottomMenu() else hideBottomMenu()
        })
    }

    private fun navigateFromAddEventToEventList() {
        findNavController(R.id.mainContainer).popBackStack()
    }

    private fun navigateToAddEvent() {
        findNavController(R.id.mainContainer)
            .navigate(R.id.action_eventListFragment_to_addEventFragment)
    }

    private fun showBottomMenu() {
        binding.mainMenu.apply {
            alpha = ANIMATION_GONE_ALPHA
            visibility = View.VISIBLE
            animate().alpha(ANIMATION_VISIBLE_ALPHA)
                .setDuration(ANIMATION_MENU_DURATION)
                .setListener(null)
        }
    }

    private fun hideBottomMenu() {
        binding.mainMenu.animate()
            .alpha(ANIMATION_GONE_ALPHA)
            .setDuration(ANIMATION_MENU_DURATION)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    binding.mainMenu.visibility = View.GONE
                }
            })
    }
}