package com.example.testtasketnetera.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.testtasketnetera.repository.sport_events_repository.SportEventsRepository
import com.example.testtasketnetera.repository.sport_events_repository.model.SortingTypeEnum
import com.example.testtasketnetera.repository.storage_repository.StorageRepository
import com.example.testtasketnetera.utils.LoggingUtils
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.kodein.di.generic.instance

/**
 * View model class for SettingsFragment
 */
class SettingsViewModel(app: Application) : BaseViewModel(app) {

    private val eventsRepository: SportEventsRepository by instance()
    private val storageRepository: StorageRepository by instance()
    private val compositeDisposable = CompositeDisposable()

    /**
     * LiveData for indicating events sorting type
     */
    private val _showEventsSortingType =
        MutableLiveData<SortingTypeEnum>().apply { eventsRepository.getCurrentSortingType() }
    val showEventsSortingType: LiveData<SortingTypeEnum> = _showEventsSortingType

    /**
     * LiveData for indicating local storage clearing state
     */
    private val _inLocalStorageClearingState = MutableLiveData<Boolean>()
    val inLocalStorageClearingState: LiveData<Boolean> = _inLocalStorageClearingState

    /**
     * LiveData for indicating local storage cleared state
     */
    private val _inLocalStorageClearedState = MutableLiveData<Boolean>()
    val inLocalStorageClearedState: LiveData<Boolean> = _inLocalStorageClearedState

    /**
     * LiveData for indicating remote storage clearing state
     */
    private val _inRemoteStorageClearingState = MutableLiveData<Boolean>()
    val inRemoteStorageClearingState: LiveData<Boolean> = _inRemoteStorageClearingState

    /**
     * LiveData for indicating remote storage cleared state
     */
    private val _inRemoteStorageClearedState = MutableLiveData<Boolean>()
    val inRemoteStorageClearedState: LiveData<Boolean> = _inRemoteStorageClearedState

    /**
     * LiveData for indicating error state on SettingsFragment
     */
    private val _inErrorState = MutableLiveData<Boolean>()
    val inErrorState: LiveData<Boolean> = _inErrorState

    /**
     * Button for clearing local storage was clicked
     */
    fun onLocalStorageClearBtnClicked() {
        _inLocalStorageClearingState.value = true
        val disposable = storageRepository.clearLocalStorage()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _inLocalStorageClearedState.value = true
            }, { error ->
                _inErrorState.value = true
                LoggingUtils.log(error)
            })
        compositeDisposable.add(disposable)
    }

    /**
     * Button for clearing remote storage was clicked
     */
    fun onRemoteStorageClearBtnClicked() {
        _inRemoteStorageClearingState.value = true
        // todo - can be implemented with Google cloud functions
    }

    /**
     * Method for getting current preference about filtering local events
     */
    fun isNeedToShowEventsFromLocalStorage(): Boolean {
        return eventsRepository.isNeedToShowEventsFromLocalStorage()
    }

    /**
     * Method to switch local events display mode
     * @param needToShow - show local events if it's true
     */
    fun setShowEventsFromLocalStorage(needToShow: Boolean) {
        eventsRepository.showEventsFromLocalStorage(needToShow)
    }

    /**
     * Method for getting current preference about filtering remote events
     */
    fun isNeedToShowEventsFromRemoteStorage(): Boolean {
        return eventsRepository.isNeedToShowEventsFromRemoteStorage()
    }

    /**
     * Method to switch remote events display mode
     * @param needToShow - show remote events if it's true
     */
    fun setShowEventsFromRemoteStorage(needToShow: Boolean) {
        eventsRepository.showEventsFromRemoteStorage(needToShow)
    }

    /**
     * Method for setting current sorting type of events
     */
    fun setSortingType(sortingType: SortingTypeEnum) {
        eventsRepository.setCurrentSortingType(sortingType)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}