package com.example.testtasketnetera.presentation.fragment

import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.example.testtasketnetera.R
import com.example.testtasketnetera.databinding.FragmentAddEventBinding
import com.example.testtasketnetera.databinding.ToolbarBinding
import com.example.testtasketnetera.presentation.viewmodel.AddEventViewModel
import com.example.testtasketnetera.presentation.viewmodel.MainViewModel
import com.example.testtasketnetera.repository.storage_repository.model.StorageTypeEnum
import com.example.testtasketnetera.utils.TextWatcherWrapper
import com.google.android.material.snackbar.Snackbar

/**
 * Fragment for saving new sport event
 */
class AddEventFragment : BaseFragment(R.layout.fragment_add_event) {

    private var _binding: FragmentAddEventBinding? = null
    private val binding get() = _binding!!

    private var _toolbarBinding: ToolbarBinding? = null
    private val toolbarBinding get() = _toolbarBinding!!

    private val viewModel: AddEventViewModel by viewModels()
    private val mainActivityViewModel: MainViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        processOnBackPressed()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentAddEventBinding.bind(view)
        _toolbarBinding = binding.toolbar

        configureToolbar()
        configureNameEditText()
        configureLocationEditText()
        configureDurationEditText()
        configureStorageTypesRadioGroup()
        configureSendBtn()
        subscribeOnViewModelEvents()
    }

    private fun configureToolbar() {
        context?.let {
            toolbarBinding.toolbar.navigationIcon = ContextCompat.getDrawable(it, R.drawable.ic_arrow_back)
            toolbarBinding.toolbar.setNavigationOnClickListener {
                mainActivityViewModel.navigateFromAddEventToEventListFragment()
            }
        }
    }

    private fun configureNameEditText() {
        binding.addEventNameEditText.addTextChangedListener(object : TextWatcherWrapper() {
            override fun onTextChanged(newValue: CharSequence?, p1: Int, p2: Int, p3: Int) {
                viewModel.currentName = newValue?.toString() ?: ""
            }
        })
    }

    private fun configureLocationEditText() {
        binding.addEventLocationEditText.addTextChangedListener(object : TextWatcherWrapper() {
            override fun onTextChanged(newValue: CharSequence?, p1: Int, p2: Int, p3: Int) {
                viewModel.currentLocationName = newValue?.toString() ?: ""
            }
        })
    }

    private fun configureDurationEditText() {
        binding.addEventDurationEditText.addTextChangedListener(object : TextWatcherWrapper() {
            override fun onTextChanged(newValue: CharSequence?, p1: Int, p2: Int, p3: Int) {
                viewModel.currentDuration = newValue?.toString() ?: ""
            }
        })
    }

    private fun configureStorageTypesRadioGroup() {
        binding.addEventStorageTypes.setOnCheckedChangeListener { radioGroup, checkedId ->
            val radioButton: RadioButton = radioGroup.findViewById(checkedId)
            if (radioButton.isChecked) {
                when (checkedId) {
                    R.id.addEventLocalType -> {
                        viewModel.storageType = StorageTypeEnum.LOCAL
                    }
                    R.id.addEventRemoteType -> {
                        viewModel.storageType = StorageTypeEnum.REMOTE
                    }
                }
            }
        }
    }

    private fun configureSendBtn() {
        binding.addEventBtn.setOnClickListener { viewModel.onSendButtonClicked() }
    }

    private fun subscribeOnViewModelEvents() {
        viewModel.inSuccessState.observe(viewLifecycleOwner, {
            it?.let { isInSuccess -> processSuccessState(isInSuccess) }
        })
        viewModel.inLoadingState.observe(viewLifecycleOwner, {
            it?.let { isInLoading -> processLoadingState(isInLoading) }
        })
        viewModel.inValidationErrorState.observe(viewLifecycleOwner, {
            it?.let { isInValidationError -> processValidationErrorState(isInValidationError) }
        })
        viewModel.inErrorState.observe(viewLifecycleOwner, {
            it?.let { isInError -> processErrorState(isInError) }
        })
    }

    private fun processSuccessState(isInSuccess: Boolean) {
        if (isInSuccess) {
            binding.addEventNameEditText.isEnabled = true
            binding.addEventLocationEditText.isEnabled = true
            binding.addEventDurationEditText.isEnabled = true
            binding.addEventStorageTypes.isEnabled = true
            binding.addEventBtn.visibility = View.VISIBLE
            binding.addEventProgress.visibility = View.GONE
            mainActivityViewModel.navigateFromAddEventToEventListFragment()
            Toast.makeText(context, R.string.fragment_add_event_success_msg, Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun processLoadingState(isInProgress: Boolean) {
        if (isInProgress) {
            binding.addEventNameEditText.isEnabled = false
            binding.addEventLocationEditText.isEnabled = false
            binding.addEventDurationEditText.isEnabled = false
            binding.addEventStorageTypes.isEnabled = false
            binding.addEventBtn.visibility = View.GONE
            binding.addEventProgress.visibility = View.VISIBLE
        }
    }

    private fun processValidationErrorState(isInValidationErrorState: Boolean) {
        if (isInValidationErrorState) {
            binding.addEventNameEditText.isEnabled = true
            binding.addEventLocationEditText.isEnabled = true
            binding.addEventDurationEditText.isEnabled = true
            binding.addEventStorageTypes.isEnabled = true
            binding.addEventBtn.visibility = View.VISIBLE
            binding.addEventProgress.visibility = View.GONE
            binding.addEventNameLayout.error =
                context?.getString(R.string.fragment_add_event_validation_error_msg)
        }
    }

    private fun processErrorState(isInError: Boolean) {
        if (isInError) {
            binding.addEventNameEditText.isEnabled = true
            binding.addEventLocationEditText.isEnabled = true
            binding.addEventDurationEditText.isEnabled = true
            binding.addEventStorageTypes.isEnabled = true
            binding.addEventBtn.visibility = View.VISIBLE
            binding.addEventProgress.visibility = View.GONE
            Snackbar.make(
                binding.addEventBtn,
                R.string.fragment_add_event_error_msg,
                Snackbar.LENGTH_LONG
            ).show()
        }
    }

    override fun onDestroyView() {
        _toolbarBinding = null
        _binding = null
        super.onDestroyView()
    }

    private fun processOnBackPressed() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            mainActivityViewModel.navigateFromAddEventToEventListFragment()
        }
    }
}