package com.example.testtasketnetera.presentation.fragment

import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import androidx.fragment.app.viewModels
import com.example.testtasketnetera.R
import com.example.testtasketnetera.databinding.FragmentSettingsBinding
import com.example.testtasketnetera.presentation.viewmodel.SettingsViewModel
import com.example.testtasketnetera.repository.sport_events_repository.model.SortingTypeEnum
import com.google.android.material.snackbar.Snackbar

/**
 * Fragment for user's settings or additional actions
 */
class SettingsFragment : BaseFragment(R.layout.fragment_settings) {

    private var _binding: FragmentSettingsBinding? = null
    private val binding get() = _binding!!

    private val viewModel: SettingsViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentSettingsBinding.bind(view)

        configureFilterCheckBoxes()
        configureSortingRadioButtons()
        configureButtons()

        subscribeOnLiveDataSortingEvents()
        subscribeOnLiveDataStateEvents()
    }

    private fun configureFilterCheckBoxes() {
        binding.settingsShowLocalCheckBox.isChecked = viewModel.isNeedToShowEventsFromLocalStorage()
        binding.settingsShowLocalCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.setShowEventsFromLocalStorage(isChecked)
        }
        binding.settingsShowRemoteCheckBox.isChecked = viewModel.isNeedToShowEventsFromRemoteStorage()
        binding.settingsShowRemoteCheckBox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.setShowEventsFromRemoteStorage(isChecked)
        }
    }

    private fun configureSortingRadioButtons() {
        binding.settingsSortRadioGroup.setOnCheckedChangeListener { radioGroup, checkedId ->
            val radioButton: RadioButton = radioGroup.findViewById(checkedId)
            if (radioButton.isChecked) {
                when (checkedId) {
                    R.id.settingsSortByCreatedDate -> {
                        viewModel.setSortingType(SortingTypeEnum.DATE)
                    }
                    R.id.settingsSortByName -> {
                        viewModel.setSortingType(SortingTypeEnum.NAME)
                    }
                    R.id.settingsSortByLocation -> {
                        viewModel.setSortingType(SortingTypeEnum.LOCATION)
                    }
                    R.id.settingsSortByDuration -> {
                        viewModel.setSortingType(SortingTypeEnum.DURATION)
                    }
                }
            }
        }
    }

    private fun configureButtons() {
        binding.settingsClearLocalStorageBtn.setOnClickListener {
            viewModel.onLocalStorageClearBtnClicked()
        }
        // todo - can be implemented with Google cloud functions
        binding.settingsClearRemoteStorageLabel.visibility = View.INVISIBLE
        binding.settingsClearRemoteStorageBtn.visibility = View.INVISIBLE
        binding.settingsClearRemoteStorageBtn.setOnClickListener {
            viewModel.onRemoteStorageClearBtnClicked()
        }
    }

    private fun subscribeOnLiveDataSortingEvents() {
        viewModel.showEventsSortingType.observe(viewLifecycleOwner, {
            it?.let { sortingTypeEnum ->
                processSortingTypeEvent(sortingTypeEnum)
            }
        })
    }

    private fun processSortingTypeEvent(sortingTypeEnum: SortingTypeEnum) {
        when (sortingTypeEnum) {
            SortingTypeEnum.DATE -> { binding.settingsSortRadioGroup.check(R.id.settingsSortByCreatedDate) }
            SortingTypeEnum.NAME -> { binding.settingsSortRadioGroup.check(R.id.settingsSortByName) }
            SortingTypeEnum.LOCATION -> { binding.settingsSortRadioGroup.check(R.id.settingsSortByLocation) }
            SortingTypeEnum.DURATION -> { binding.settingsSortRadioGroup.check(R.id.settingsSortByDuration) }
        }
    }

    private fun subscribeOnLiveDataStateEvents() {
        viewModel.inLocalStorageClearingState.observe(viewLifecycleOwner, {
            it?.let { processLocalStorageClearingState() }
        })
        viewModel.inLocalStorageClearedState.observe(viewLifecycleOwner, {
            it?.let { processLocalStorageClearedState() }
        })
        viewModel.inRemoteStorageClearingState.observe(viewLifecycleOwner, {
            it?.let { processRemoteStorageClearingState() }
        })
        viewModel.inRemoteStorageClearedState.observe(viewLifecycleOwner, {
            it?.let { processRemoteStorageClearedState() }
        })
        viewModel.inErrorState.observe(viewLifecycleOwner, {
            it?.let { processErrorState() }
        })
    }

    private fun processLocalStorageClearingState() {
        binding.settingsClearLocalStorageBtn.visibility = View.INVISIBLE
        binding.settingsClearLocalStorageProgress.visibility = View.VISIBLE
    }

    private fun processLocalStorageClearedState() {
        binding.settingsClearLocalStorageBtn.visibility = View.VISIBLE
        binding.settingsClearLocalStorageProgress.visibility = View.INVISIBLE
        Snackbar.make(binding.settingsClearRemoteStorageBtn, R.string.fragment_settings_local_cleared_msg, Snackbar.LENGTH_SHORT)
            .show()
    }

    private fun processRemoteStorageClearingState() {
        binding.settingsClearRemoteStorageBtn.visibility = View.INVISIBLE
        binding.settingsClearRemoteStorageProgress.visibility = View.VISIBLE
    }

    private fun processRemoteStorageClearedState() {
        binding.settingsClearRemoteStorageBtn.visibility = View.VISIBLE
        binding.settingsClearRemoteStorageProgress.visibility = View.INVISIBLE
        Snackbar.make(binding.settingsClearRemoteStorageBtn, R.string.fragment_settings_remote_cleared_msg, Snackbar.LENGTH_SHORT)
            .show()
    }

    private fun processErrorState() {
        binding.settingsClearLocalStorageBtn.visibility = View.VISIBLE
        binding.settingsClearLocalStorageProgress.visibility = View.INVISIBLE
        binding.settingsClearRemoteStorageBtn.visibility = View.VISIBLE
        binding.settingsClearRemoteStorageProgress.visibility = View.INVISIBLE
        Snackbar.make(binding.settingsClearRemoteStorageBtn, R.string.fragment_settings_error_msg, Snackbar.LENGTH_LONG)
            .show()
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }
}