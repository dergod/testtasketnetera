package com.example.testtasketnetera.presentation.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testtasketnetera.R
import com.example.testtasketnetera.databinding.FragmentEventListBinding
import com.example.testtasketnetera.presentation.adapter.EventsAdapter
import com.example.testtasketnetera.presentation.model.SportEventPresentationModel
import com.example.testtasketnetera.presentation.viewmodel.EventListViewModel
import com.example.testtasketnetera.presentation.viewmodel.MainViewModel
import com.google.android.material.snackbar.Snackbar

/**
 * Fragment for the list of sport events
 */
class EventListFragment : BaseFragment(R.layout.fragment_event_list) {

    private var _binding: FragmentEventListBinding? = null
    private val binding get() = _binding!!

    private val viewModel: EventListViewModel by viewModels()
    private val mainActivityViewModel: MainViewModel by activityViewModels()
    private var adapter: EventsAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentEventListBinding.bind(view)

        configureRecycler()
        configureFab()
        subscribeOnViewModelEvents()
    }

    private fun configureRecycler() {
        adapter = EventsAdapter()
        adapter?.context = context
        binding.eventListRecycler.adapter = adapter
        binding.eventListRecycler.layoutManager = LinearLayoutManager(context)
    }

    private fun configureFab() {
        binding.eventListFab.setOnClickListener {
            mainActivityViewModel.navigateToAddEventFragment()
        }
    }

    private fun subscribeOnViewModelEvents() {
        viewModel.requestData()
        viewModel.inDataState.observe(viewLifecycleOwner, {
            it?.let { data -> processDataState(data) }
        })
        viewModel.inLoadingState.observe(viewLifecycleOwner, {
            it?.let { isInProgress -> processProgressState(isInProgress) }
        })
        viewModel.inErrorState.observe(viewLifecycleOwner, {
            it?.let { isInError -> processErrorState(isInError) }
        })
    }

    private fun processDataState(data: List<SportEventPresentationModel>) {
        binding.eventListProgress.visibility = View.GONE
        if (data.isNullOrEmpty()) {
            binding.eventListRecycler.visibility = View.GONE
            binding.eventListEmptyMsg.visibility = View.VISIBLE
        } else {
            binding.eventListRecycler.visibility = View.VISIBLE
            binding.eventListEmptyMsg.visibility = View.GONE
            adapter?.events = data
            adapter?.notifyDataSetChanged() // todo - it's needed to fix to DiffUtils
        }
    }

    private fun processProgressState(isInProgress: Boolean) {
        if (isInProgress) {
            binding.eventListProgress.visibility = View.VISIBLE
            binding.eventListRecycler.visibility = View.GONE
            binding.eventListEmptyMsg.visibility = View.GONE
        }
    }

    private fun processErrorState(isInError: Boolean) {
        if (isInError) {
            binding.eventListProgress.visibility = View.GONE
            binding.eventListRecycler.visibility = View.GONE
            binding.eventListEmptyMsg.visibility = View.VISIBLE
            Snackbar
                .make(
                    binding.eventListRecycler,
                    R.string.fragment_event_list_error_msg,
                    Snackbar.LENGTH_INDEFINITE
                )
                .setAction(R.string.fragment_event_list_retry_btn) { viewModel.requestData() }
                .show()
        }
    }

    override fun onDestroy() {
        adapter?.context = null
        _binding = null
        super.onDestroy()
    }
}