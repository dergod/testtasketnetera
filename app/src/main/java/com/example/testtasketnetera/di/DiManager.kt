package com.example.testtasketnetera.di

import android.app.Application
import com.example.testtasketnetera.data.db.DatabaseManager
import com.example.testtasketnetera.data.network.firestore_api.FirestoreApiClient
import com.example.testtasketnetera.data.preferences.PreferencesManager
import com.example.testtasketnetera.repository.location_repository.LocationRepository
import com.example.testtasketnetera.repository.location_repository.LocationRepositoryImpl
import com.example.testtasketnetera.repository.sport_events_repository.SportEventsRepository
import com.example.testtasketnetera.repository.sport_events_repository.SportEventsRepositoryImpl
import com.example.testtasketnetera.repository.storage_repository.StorageRepository
import com.example.testtasketnetera.repository.storage_repository.StorageRepositoryImpl
import org.kodein.di.Kodein
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

/**
 * Singleton - helper class for DI
 */
object DiManager {

    private const val DATA_MODULE_NAME = "DATA_MODULE"
    private const val REPOSITORY_MODULE_NAME = "REPOSITORY_MODULE"

    private lateinit var dataModule: Kodein.Module
    private lateinit var repositoryModule: Kodein.Module

    /**
     * Initialization method
     * @return kodein instance
     */
    fun init(app: Application): Kodein {
        initDataModule()
        initRepositoryModule()
        return Kodein {
            import(androidXModule(app))
            import(dataModule)
            import(repositoryModule)
        }
    }

    /**
     * Initialization method for data layer entities
     */
    private fun initDataModule() {
        dataModule = Kodein.Module(DATA_MODULE_NAME) {
            bind() from singleton { DatabaseManager(instance()) }
            bind() from singleton { FirestoreApiClient() }
            bind() from singleton { PreferencesManager(instance()) }
        }
    }

    /**
     * Initialization method for repositories
     */
    private fun initRepositoryModule() {
        repositoryModule = Kodein.Module(REPOSITORY_MODULE_NAME) {
            bind<SportEventsRepository>() with singleton {
                SportEventsRepositoryImpl(instance(), instance(), instance())
            }
            bind<StorageRepository>() with singleton { StorageRepositoryImpl(instance()) }
            bind<LocationRepository>() with singleton { LocationRepositoryImpl() }
        }
    }
}